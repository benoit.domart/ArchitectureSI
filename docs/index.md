# Architecture des SI

## [Premier projet : Boutique en ligne.](https://benoit.domart.gitlab.io/ArchitectureSI/1-boutique/)

L'objectif est ici de créer un premier projet Web, qui respecte le paradigme MVC (modèle, vue, contrôleur).
Nous allons commencer par mettre en place 

* Il faut commencer par télécharger eclipse.

# Welcome to MkDocs

This is a test site hosted on [GitLab Pages](https://pages.gitlab.io). You can
[browse its source code](https://gitlab.com/pages/mkdocs), fork it and start
using it on your projects.

For full documentation visit [mkdocs.org](http://mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
