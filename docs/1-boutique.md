<link rel="stylesheet" type="text/css" media="all" href="/ArchitectureSI/css/monStyle.css" />

# Premier projet : Boutique en ligne.

L'objectif est ici de créer un premier projet Web, qui respecte le paradigme MVC (modèle, vue, contrôleur).

## 1. Présentation du projet

L'idée est d'obtenir un site web dynamique, en fait une boutique en ligne, où l'on peut sélectionner différents objets (dans cette première version, uniquement des stylos, des feutres et des gommes), en quantité souhaitée. Notre application indique ensuite le coût du panier, les éventuels frais de livraison, et le coût total de la commande. Trois règles de gestions (RG) doivent pour cela être mises en place :

* RG1 : Calcul du prix du panier, sachant que :
    * un styo coûte 1,20€,
    * un feutre coûte 1,80€,
    * une gomme coûte 2€.
* RG2 : Les frais de livraison sont de 5€ si la commande est de moins de 15€, et sont offerts sinon.
* RG3 : Le prix total de la commande est la somme du prix du panier et des évenutels frais de livraison.

Voici la page (il n'y en a qu'une) de notre site :
<center>![1er test](images/BoutiqueWebV1_a.png)</center>

Le client peut sélectionner le nombre de chaque objet désiré. Lorsqu'il clique sur `Valider`, la partie `Récapitulatif du panier` se met à jour, en appliquant les 3 RG définies ci-dessus :
<center>![1er test](images/BoutiqueWebV1_b.png)</center>


## 2. Installation de l'environnement de développement

Première chose à faire avant de se lancer dans le développement : installer l'environnement de développement. Pour cela, créer quelque part un dossier `jee` dans lequel nous allons placer tous les outils nécessaires au developpement de notre projet. Pour cette première version de notre site, nous avons besoin de trois choses :

* La dernière version d'**eclipse**.<br>On la trouve ici : [https://www.eclipse.org/downloads/](https://www.eclipse.org/downloads/). Attention, il faut bien sélectionner "Eclipse IDE for Entreprise Java and Web Developers" comme dans la capture d'écran ci-dessous :
  <center>![1er test](images/install_eclipse.png)</center>
* La dernière version de la **jdk**.<br>On la trouve ici : [https://www.oracle.com/java/technologies/downloads](https://www.oracle.com/java/technologies/downloads).
* Une version de **tomcat**.<br>Ici, nous n'allons pas télécharger la dernière version (la 10 actuellement), car elle implémente Jakarta EE 9. Nous allons télécharger la version tomcat9 (disponible ici : [https://tomcat.apache.org/download-90.cgi](https://tomcat.apache.org/download-90.cgi)), qui implémente Java EE 8. De manière générale, il peut être judicieux de ne pas utiliser les toutes dernières versions des produits, qui peuvent contenenir des bugs non encore connus (de sécurité notamment) ...<br/>
  Il suffit de télécharger le zip (si on travaille avec windows), et de le dézipper dans le dossier `jee`.


## 3. Configuration de l'environnement de développement

1. On commence par lancer eclipse, en indiquand comme espace de travail (worspace) `jee/ws_boutique`.
2. Dans eclipse, on sélectionne la dernière jdk comme jre par défaut.
    * Pour cela, on va dans le menu `Window > Preferences`, puis dans l'arborescance `Java > Installed JREs`.
    * On clique sur `Add`, puis _Standard VM_ puis on clique sur `Directory` pour indiquer le dossier où est présente la jdk.
    * Dans la liste des jres, on coche la case de cette jdk pour que ce soit l'environnement par défaut.
2. Nous allons développer une application JEE, déployée sur un serveut tomcat 9. Eclipse nous permet de définir un **environnement d'éxécution** (_**runtime environment**_ en anglais). Ainsi, en associant un projet donné à un _runtime_ donné, toutes les bibliothèques nécessaires sont automatiques ajoutées au projet.
    * Pour cela, dans eclipse, on va dans le menu `Window > Preferences`, puis dans l'arborescance `Serveur > Runtime Environments`.
    * Dans la liste, sélectionner `Apache > Apache Tomcat v9.0` puis `next`.
    * Cliquer sur `Browse...` pour indiquer le dosser où tomcat a été dézippé.
    * On peut enfin cliquer sur `Finish`.


## 4. Conception de l'application

Notre application doit respecter le paradigme MVC. Ici, elle est très simple, et cela va rendre notre code plus complexe. Mais cela nous fera gagner beaucoup de temps par la suite, lorsqu'elle va évoluer ! Si tout le monde respecte les mêmes normes, le code est _rangé_ de la même façon pour tout le monde, et on s'y retrouve donc beaucoup plus rapidement dans le code de quelqu'un d'autre lorsqu'il faut ajouter une fonctionnalité ou corriger un bug dans une application développée par quelqu'un d'autre.

Pour l'instant, il faut donc déterminer ce qui fait partie du modèle, de la vue, et du controlêur :

1. **Le modèle**<br/>
  Il n'y a pour l'instant qu'une seule classe. C'est le panier. C'est un **_bean_** Java, qui contient plusieurs attributs (et les _getters_/_setters_ associés) :
    * le nombre de stylos commandés,
    * le nombre de feutres commandés,
    * le nombre de gommes commandées,
    * le prix du panier,
    * les frais de livraison,
    * et le prix total de la commande.
2. **La vue**<br/>
  C'est l'affichage de la page Web permettant de sélectionner les quantités désirées de ces trois produits, et qui indique le prix du panier, les frais de livraison et le coût total de la commande.
3. **Le contrôleur**<br/>
  C'est lui qui affiche la vue (avec aucun produit sélectionné initialement) le première fois, et qui met à jour le modèle, avec les informations saisies par l'utilisateur, puis qui ré-affiche la vue mise à jour.<br/>
4. Il y a en fait une 4ème couche à notre application : **La partie métier**<br/>
  Elle est appelée par le contrôleur lorsque l'utilisateur a saisi des données. Elle s'occuupe de l'implémentation des trois règles de gestions citées plus haut.


## Développement de l'application
